package com.cnb.dsa.cache;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LRUCacheTest {

    GenericLRUCache<Integer, Integer> cache;

    @Before
    public void setUp(){
         cache = new GenericLRUCache(3);

        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
    }

    @Test
    public void testGetMovesToHead(){

        int one = cache.get(1);

        assertEquals(1, one);

        assertEquals(Integer.valueOf(1), cache.head.value);
    }

    @Test
    public void testHead(){

        assertEquals(Integer.valueOf(3), cache.head.value);
    }

    @Test
    public void testTail(){

        assertEquals(Integer.valueOf(1), cache.tail.value);
    }

    @Test
    public void testOverflow(){

        cache.put(4, 4);

        assertEquals(Integer.valueOf(4), cache.head.value);

        assertEquals(null, cache.get(1));

        assertEquals(Integer.valueOf(2), cache.tail.value);
    }
}
