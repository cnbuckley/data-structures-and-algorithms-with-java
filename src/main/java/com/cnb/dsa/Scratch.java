package com.cnb.dsa;


import java.util.Arrays;

public class Scratch {

    public static void main(String[] args){

        int[] input = {6,9,3,1,2,7,5,4,8};

        Scratch scratch = new Scratch();
        scratch.linearithmic(100);

    }


    /**
     * linear (N + N/2 + N/4 + ...);
     * @param N
     */
    public void linear(int N){
        int sum = 0;
        for (int n = N; n > 0; n /= 2) {
            for (int i = 0; i < n; i++) {
                System.out.println("n = "+n+", i="+i);
                sum++;
            }
        }

        System.out.println(sum);
    }

    /**
     * linear(1 + 2 + 4 + 8 + 16 + ...);
     * @param N
     */
    public void linearA(int N){
        int sum = 0;
        for (int i = 1; i < N; i *= 2){
            for(int j = 0; j < i; j++){
                print(j, i);
                sum++;
            }
        }

        System.out.println(sum);
    }

    public void linearithmic(int N){
        int sum = 0;
        for (int i = 1; i < N; i *= 2) {
            for (int j = 0; j < N; j++) {
                print(j, i);
                sum++;
            }
        }

        System.out.println(sum);
    }

    private void print(int i, int j){
        System.out.println("j = "+j+", i="+i);
    }
}
