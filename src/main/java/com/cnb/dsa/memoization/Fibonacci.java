package com.cnb.dsa.memoization;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

    Map<Integer, Long> memo = new HashMap<>();


    public static void main(String[] args){
        Fibonacci fibonacci = new Fibonacci();
        System.out.println(fibonacci.calculate(10));
    }

    public long calculate(int n){
        if(memo.containsKey(n)){
            return memo.get(n);
        }

        if(n<=2){
            return 1L;
        }

        long result = calculate(n - 2 ) + calculate( n - 1 );

        memo.put(n, result);

        return result;
    }
}
