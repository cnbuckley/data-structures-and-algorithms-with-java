package com.cnb.dsa;

public class HashNode<K, V>
{
    public K key;
    public V value;

    // Reference to next node
    public HashNode<K, V> next;

    // Constructor
    public HashNode(K key, V value)
    {
        this.key = key;
        this.value = value;
    }
}
