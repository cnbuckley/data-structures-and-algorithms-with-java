package com.cnb.dsa.tree;

import com.cnb.dsa.TreeNode;

public class BinaryTree {

    TreeNode root;

    public static void main(String[] args){

        BinaryTree tree = new BinaryTree();

        tree.add(5);
        tree.add(6);
        tree.add(7);
        tree.add(4);
        tree.add(3);

        int[] inputs = {5,6,7,8,9,10};
        int middle = inputs.length / 2;

        System.out.println(inputs[middle] +" able to be balanced "+(inputs.length % 2 == 0 ? false : true));
    }

    private TreeNode addRecursive(TreeNode current, int value) {
        if (current == null) {
            return new TreeNode(value);
        }

        if (value < current.value) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        } else {
            // value already exists
            return current;
        }

        return current;
    }

    public void add(int value) {
        root = addRecursive(root, value);
    }

    private boolean containsTreeNodeRecursive(TreeNode current, int value) {
        if (current == null) {
            return false;
        }
        if (value == current.value) {
            return true;
        }
        return value < current.value
                ? containsTreeNodeRecursive(current.left, value)
                : containsTreeNodeRecursive(current.right, value);
    }

    public boolean containsTreeNode(int value) {
        return containsTreeNodeRecursive(root, value);
    }

}
