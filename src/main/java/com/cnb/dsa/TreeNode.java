package com.cnb.dsa;

public class TreeNode {

    public int value;

    public TreeNode left, right;

    public TreeNode(int value){
        this.value = value;
    }

}
