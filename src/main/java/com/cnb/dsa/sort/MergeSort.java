package com.cnb.dsa.sort;


public class MergeSort {

    public static void main(String[] args){
        int[] actual = { 5, 1, 55, 6 };

        MergeSort sorter = new MergeSort();
        sorter.mergesort(actual, new int[actual.length], 0, actual.length - 1);
    }

    public void mergesort(int[] arr, int[] temp, int begin, int end){
        if(begin >= end){
            return;
        }

        int middle = (begin + end) / 2;

        mergesort(arr, temp, begin, middle);
        mergesort(arr, temp, middle+1, end);

        mergeHalves(arr, begin, end);
    }

    public void mergeHalves(int[] arr, int begin, int end){

    }

}
