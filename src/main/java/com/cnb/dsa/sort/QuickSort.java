package com.cnb.dsa.sort;

import java.util.Arrays;

public class QuickSort {


    public static void main(String[] args){

        int[] inputs = { 9,3,7,12,33,2,4,1,6,5,11,20 };

        QuickSort quickSort = new QuickSort();
        quickSort.quicksort(inputs, 0, inputs.length-1);

        System.out.println(Arrays.toString(inputs));
    }


    public void quicksort(int[] arr, int begin, int end){

        System.out.println("start->" + Arrays.toString(arr) + " begin="+begin+", end="+end);

        if(begin <= end){

            int partitionIndex = partition(arr, begin, end);

            quicksort(arr, begin, partitionIndex-1);
            quicksort(arr, partitionIndex+1, end);
        }
    }

    private int partition(int[] arr, int begin, int end){
        int pivot = arr[end];
        int i = (begin-1);

        for(int j = begin; j < end; j++){

            if( arr[j] <= pivot ){
                i++;

                swap(arr, i, j);
            }
        }

        //swap the pivot, the pivot is now in it's correct position
        swap(arr, i+1, end);

        return i+1;
    }

    private void swap(int[] arr, int high, int low){
        int temp = arr[high];

        arr[high] = arr[low];
        arr[low] = temp;
    }
}
