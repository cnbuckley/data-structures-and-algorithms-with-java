package com.cnb.dsa.sort;

import java.util.Arrays;
import java.util.stream.Collectors;

public class BubbleSort {

    public static void main(String[] args) {

        int[] array = {99, 88, 55, 77, 1, 66};

        System.out.print("unsorted data: ");
        printArray(array);

        System.out.print("ascending order: "); //1,55,66,77,88,99
        bubble_sort(array);

        printArray(array);

        System.out.print("descending order: "); //99,88,77,66,55,1
        bubble_sort(array, false);

        printArray(array);

    }

    private static void bubble_sort(int[] input) {
        bubble_sort(input, true);
    }

    private static void bubble_sort(int[] input, boolean ascending) {

        int length = input.length;
        int temp;
        boolean sorted;

        for(int i = 0; i < length; i++){

            sorted = true;

            for(int j = 1; j < (length - i); j++){

                if(input[j] > input[j - 1]){
                    temp = input[j];
                    input[j] = input[j - 1];
                    input[j - 1] = temp;
                    sorted = false;
                }
            }

            if(sorted){
                break;
            }
        }

    }

    private static void printArray(int[] data) {
        String result = Arrays.stream(data)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(","));
        System.out.println(result);
    }

}

