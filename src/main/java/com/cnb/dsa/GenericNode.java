package com.cnb.dsa;

public class GenericNode<K, V> {

    public K key;
    public V value;

    public GenericNode<K, V> next, prev;

    public GenericNode(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
