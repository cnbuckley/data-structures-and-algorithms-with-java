package com.cnb.dsa.cache;

import com.cnb.dsa.Node;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {

    int capacity;

    Node currentHead, currentTail;
    Map<Integer, Node> map;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
    }

    public int get(int key){

        if(map.containsKey(key)){
            Node found = map.get(key);

            remove(found);
            addToHead(found);

            return found.value;
        }

        return -1;
    }

    public void put(int key, int value){

        if(map.containsKey(key)){
            Node found = map.get(key);
            found.value = value;

            remove(found);
            addToHead(found);
        } else {
            Node insert = new Node(key, value);

            map.put(key, insert);

            if(map.size() > capacity){
                map.remove(currentTail.key);
                remove(currentTail);
                addToHead(insert);
            } else {
                addToHead(insert);
            }

        }
    }

    private void addToHead(Node newHead){
        newHead.next = currentHead;
        newHead.prev = null;
        if (currentHead != null) {
            currentHead.prev = newHead;
        }
        currentHead = newHead;
        if (currentTail == null) {
            currentTail = currentHead;
        }
    }

    private void remove(Node node){
        if(node.prev!=null){
            node.prev.next = node.next;
        }else{
            currentHead = node.next;
        }

        if(node.next!=null){
            node.next.prev = node.prev;
        }else{
            currentTail = node.prev;
        }
    }
}
