package com.cnb.dsa.cache;

import com.cnb.dsa.GenericNode;

import java.util.HashMap;
import java.util.Map;

public class GenericLRUCache<K, V> {

    GenericNode<K, V> head, tail;
    Map<K, GenericNode<K, V>> map;
    int capacity;

    public GenericLRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>(capacity);
    }

    public V get(K key){

        if(map.containsKey(key)){

            GenericNode<K, V> found = map.get(key);

            remove(found);
            addToHead(found);

            return found.value;
        }

        return null;
    }

    public void put(K key, V value){
        if(map.containsKey(key)){

            GenericNode<K, V> found = map.get(key);
            found.value = value;

            remove(found);
            addToHead(found);
        } else {

            GenericNode<K, V> node = new GenericNode<>(key, value);
            map.put(key, node);

            if(map.size() > capacity){

                map.remove(tail.key);
                remove(tail);
                addToHead(node);
            } else {
                addToHead(node);
            }
        }
    }

    private void remove(GenericNode<K,V> node){
        if(node.prev != null){
            node.prev.next = node.next;
        }else{
            head = node.next;
        }

        if(node.next != null){
            node.next.prev = node.prev;
        }else{
            tail = node.prev;
        }
    }

    private void addToHead(GenericNode<K,V> node){
        node.next = head;
        node.prev = null;

        if(head != null){
            head.prev = node;
        }

        head = node;

        if(tail == null){
            tail = node;
        }
    }
}
