# data-structures-and-algorithms-with-java

Data Structures and Algorithms with the Java programming language.  I'm using primitives to create the class data structures and algorithms

# LRU Cache
The LRU cache is a hash table of keys and double linked nodes. The hash table makes the time of get() to be O(1). The list of double linked nodes make the nodes adding/removal operations O(1).